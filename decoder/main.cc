#include <cstdio>
#include "decoder.h"


#include  <netdb.h>

/** @brief Program working mode enumeration */

enum ProgramMode {
    STANDARD_MODE         = 0,
    READ_FROM_BINARY_FILE = 1,
    SAVE_TO_BINARY_FILE   = 2,
};

/** @brief Interrupt flag */

static volatile int gSigintFlag = 0;

/**
 * @brief Handle SIGINT to clean up
 *
 */

static void sigint_handler(int val)
{
    gSigintFlag = 1;
}

struct ArgumentSocket {
    std::string host;
    int port;
};
static ArgumentSocket split_string(std::string socket) {
    ArgumentSocket nsocket = {};
    size_t colonPos = socket.find(':');

    if(colonPos != std::string::npos)
    {
        std::string hostPart = socket.substr(0,colonPos);
        std::string portPart = socket.substr(colonPos+1);

        std::stringstream parser(portPart);

        int port = 0;
        if( parser >> port )
        {
            // hostname in hostPart, port in port
            // could check port >= 0 and port < 65536 here
            std::cout << "hostname in hostPart, port in port " << hostPart << " " << portPart << std::endl;
            
            struct addrinfo hints, *res;
            int status;

            memset(&hints, 0, sizeof hints);
            hints.ai_family = AF_UNSPEC; // AF_INET or AF_INET6 to force version
            hints.ai_socktype = SOCK_STREAM;
            if ((status = getaddrinfo(hostPart.c_str(), NULL, &hints, &res)) != 0) {
                fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
                //return 2;
                exit(2);
            }


            sockaddr_in *addr = (struct sockaddr_in *)res->ai_addr; 
            std::cout << "resolved address : " << inet_ntoa((struct in_addr)addr->sin_addr) << std::endl;


            nsocket.host = inet_ntoa((struct in_addr)addr->sin_addr);
            nsocket.port = atoi(portPart.c_str());
        }
        else
        {
            // port not convertible to an integer
            std::cout << "port not convertible to an integer " << hostPart << " " << portPart << std::endl;

        }
    }
    else
    {
        // Missing port?
        std::cout << "Missing port? " << std::endl;
        nsocket.host = socket;
    }

    return nsocket;
}

/**
 * @brief Decoder program entry point
 *
 * Reads demodulated values from UDP port 42000 coming from physical demodulator
 * Writes decoded frames to UDP port 42100 to tetra interpreter
 *
 * Filtering log for SDS: sed -n '/SDS/ p' log.txt > out.txt
 *
 */

int main(int argc, char * argv[])
{
    // connect interrupt Ctrl-C handler
    struct sigaction sa;
    sa.sa_handler = sigint_handler;
    sigaction(SIGINT, &sa, 0);

    ArgumentSocket argRSocket = {"127.0.0.1", 5000};
    ArgumentSocket argTSocket = {"127.0.0.1", 5001};

    const int FILENAME_LEN = 256;
    char optFilenameInput[FILENAME_LEN]  = "";                                  // input bits filename
    char optFilenameOutput[FILENAME_LEN] = "";                                  // output bits filename

    int programMode = STANDARD_MODE;
    int debugLevel = 0;
    bool bRemoveFillBits = true;

    int option;

    std::string debug;
    while ((option = getopt(argc, argv, "hr:t:i:o:d:f:")) != -1)
    {
        switch (option)
        {
        case 'r':
        {
            ArgumentSocket tmpRSocket = split_string(std::string(optarg));
            argRSocket.host = tmpRSocket.host!="" ? tmpRSocket.host : argRSocket.host;
            argRSocket.port = tmpRSocket.port!=0 ? tmpRSocket.port : argRSocket.port;
            break;
        }

        case 't': {
            ArgumentSocket tmpTSocket = split_string(std::string(optarg));
            argTSocket.host = tmpTSocket.host!="" ? tmpTSocket.host : argTSocket.host;
            argTSocket.port = tmpTSocket.port!=0 ? tmpTSocket.port : argTSocket.port;
            break;
        }

        case 'i':
            strncpy(optFilenameInput, optarg, FILENAME_LEN - 1);
            programMode |= READ_FROM_BINARY_FILE;
            break;

        case 'o':
            strncpy(optFilenameOutput, optarg, FILENAME_LEN - 1);
            programMode |= SAVE_TO_BINARY_FILE;
            break;

        case 'd':
            debugLevel = atoi(optarg);
            break;

        case 'f':
            bRemoveFillBits = false;
            break;

        case 'h':
            printf("\nUsage: ./decoder [OPTIONS]\n\n"
                   "Options:\n"
                   "  -r <UDP socket> receiving from phy [default port is 42000]\n"
                   "  -t <UDP socket> sending Json data [default port is 42100]\n"
                   "  -i <file> replay data from binary file instead of UDP\n"
                   "  -o <file> record data to binary file (can be replayed with -i option)\n"
                   "  -d <level> print debug information\n"
                   "  -f keep fill bits\n"
                   "  -h print this help\n\n");
            exit(EXIT_FAILURE);
            break;

        case '?':
            printf("unkown option, run ./decoder -h to list available options\n");
            exit(EXIT_FAILURE);
            break;
        }
    }


    // create output destination socket
    struct sockaddr_in addr_output;
    memset(&addr_output, 0, sizeof(struct sockaddr_in));
    addr_output.sin_family = AF_INET;
    addr_output.sin_port = htons(argTSocket.port);
    inet_aton(argTSocket.host.c_str(), &addr_output.sin_addr);

    int udpSocketFd  = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    connect(udpSocketFd, (struct sockaddr *) & addr_output, sizeof(struct sockaddr));
    printf("Output socket 0x%04x on port %s:%d\n", udpSocketFd, argTSocket.host.c_str(), argTSocket.port);
    if (udpSocketFd < 0)
    {
        perror("Couldn't create output socket");
        exit(EXIT_FAILURE);
    }

    // create decoder
    Tetra::LogLevel logLevel;
    switch (debugLevel)
    {
    case 0:
        logLevel = Tetra::LogLevel::NONE;
        break;
    case 1:
        logLevel = Tetra::LogLevel::LOW;
        break;
    case 2:
        logLevel = Tetra::LogLevel::MEDIUM;
        break;
    case 3:
        logLevel = Tetra::LogLevel::HIGH;
        break;
    case 4:
        logLevel = Tetra::LogLevel::VERYHIGH;
        break;
    default:
        logLevel = Tetra::LogLevel::LOW;

    }

    // output file if any
    int fdOutputSaveFile = 0;

    if (programMode & SAVE_TO_BINARY_FILE)
    {
        // save input bits to file
        fdOutputSaveFile = open(optFilenameOutput, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP);
        if (fdOutputSaveFile < 0)
        {
            fprintf(stderr, "Couldn't open output file");
            exit(EXIT_FAILURE);
        }
    }

    // input source
    int fdInput = 0;

    if (programMode & READ_FROM_BINARY_FILE)
    {
        // read input bits from file
        fdInput = open(optFilenameInput, O_RDONLY);

        printf("Input from file '%s' 0x%04x\n", optFilenameInput, fdInput);

        if (fdInput < 0)
        {
            fprintf(stderr, "Couldn't open input bits file");
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        // read input bits from UDP socket
        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(struct sockaddr_in));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(argRSocket.port);
        inet_aton(argRSocket.host.c_str(), &addr.sin_addr);

        fdInput = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        bind(fdInput, (struct sockaddr *)&addr, sizeof(struct sockaddr));

        printf("Input socket 0x%04x on port %s:%d\n", fdInput, argRSocket.host.c_str() ,argRSocket.port);

        if (fdInput < 0)
        {
            fprintf(stderr, "Couldn't create input socket");
            exit(EXIT_FAILURE);
        }
    }

    // create decoder
    Tetra::TetraDecoder * decoder = new Tetra::TetraDecoder(udpSocketFd, bRemoveFillBits, logLevel);

    // receive buffer
    const int RXBUF_LEN = 1024;
    uint8_t rxBuf[RXBUF_LEN];

    while (!gSigintFlag)
    {
        int bytesRead = read(fdInput, rxBuf, sizeof(rxBuf));

        if (errno == EINTR)
        {
            // print is required for ^C to be handled
            fprintf(stderr, "EINTR\n");
            break;
        }
        else if (bytesRead < 0)
        {
            fprintf(stderr, "Read error\n");
            break;
        }
        else if (bytesRead == 0)
        {
            break;
        }

        if (programMode & SAVE_TO_BINARY_FILE)
        {
            write(fdOutputSaveFile, rxBuf, bytesRead);
        }

        // bytes must be pushed one at a time into decoder
        for (int cnt = 0; cnt < bytesRead; cnt++)
        {
            decoder->rxSymbol(rxBuf[cnt]);
        }
    }

    close(udpSocketFd);

    // file or socket must be closed
    close(fdInput);

    // close save file only if openede
    if (programMode & SAVE_TO_BINARY_FILE)
    {
        close(fdOutputSaveFile);
    }

    delete decoder;

    printf("Clean exit\n");

    return EXIT_SUCCESS;
}
